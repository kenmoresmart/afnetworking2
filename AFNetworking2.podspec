Pod::Spec.new do |s|
  s.name     = 'AFNetworking2'
  s.version  = '2.6.3'
  s.license  = 'MIT'
  s.summary  = 'A delightful iOS and OS X networking framework.'
  s.homepage = 'https://bitbucket.org/kenmoresmart/AFNetworking2'
  s.social_media_url = 'https://twitter.com/AFNetworking2'
  s.authors  = { 'Mattt Thompson' => 'm@mattt.me' }
  s.source   = { :git => 'https://bitbucket.org/kenmoresmart/AFNetworking2.git', :tag => s.version, :submodules => true }
  s.requires_arc = true

  s.public_header_files = 'AFNetworking2/AFNetworking.h'
  s.source_files = 'AFNetworking2/AFNetworking.h'

  pch_AF = <<-EOS
#ifndef TARGET_OS_IOS
  #define TARGET_OS_IOS TARGET_OS_IPHONE
#endif

#ifndef TARGET_OS_WATCH
  #define TARGET_OS_WATCH 0
#endif
EOS
  s.prefix_header_contents = pch_AF

  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.9'
  s.watchos.deployment_target = '2.0'

  s.subspec 'Serialization' do |ss|
    ss.source_files = 'AFNetworking2/AFURL{Request,Response}Serialization.{h,m}'
    ss.public_header_files = 'AFNetworking2/AFURL{Request,Response}Serialization.h'
    ss.watchos.frameworks = 'MobileCoreServices', 'CoreGraphics'
    ss.ios.frameworks = 'MobileCoreServices', 'CoreGraphics'
    ss.osx.frameworks = 'CoreServices'
  end

  s.subspec 'Security' do |ss|
    ss.source_files = 'AFNetworking2/AFSecurityPolicy.{h,m}'
    ss.public_header_files = 'AFNetworking2/AFSecurityPolicy.h'
    ss.frameworks = 'Security'
  end

  s.subspec 'Reachability' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.osx.deployment_target = '10.9'

    ss.source_files = 'AFNetworking2/AFNetworkReachabilityManager.{h,m}'
    ss.public_header_files = 'AFNetworking2/AFNetworkReachabilityManager.h'

    ss.frameworks = 'SystemConfiguration'
  end

  s.subspec 'NSURLConnection' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.osx.deployment_target = '10.9'

    ss.dependency 'AFNetworking2/Serialization'
    ss.dependency 'AFNetworking2/Reachability'
    ss.dependency 'AFNetworking2/Security'

    ss.source_files = 'AFNetworking2/AFURLConnectionOperation.{h,m}', 'AFNetworking2/AFHTTPRequestOperation.{h,m}', 'AFNetworking2/AFHTTPRequestOperationManager.{h,m}'
    ss.public_header_files = 'AFNetworking2/AFURLConnectionOperation.h', 'AFNetworking2/AFHTTPRequestOperation.h', 'AFNetworking2/AFHTTPRequestOperationManager.h'
  end

  s.subspec 'NSURLSession' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.osx.deployment_target = '10.9'
    ss.watchos.deployment_target = '2.0'

    ss.dependency 'AFNetworking2/Serialization'
    ss.ios.dependency 'AFNetworking2/Reachability'
    ss.osx.dependency 'AFNetworking2/Reachability'
    ss.dependency 'AFNetworking2/Security'

    ss.source_files = 'AFNetworking2/AF{URL,HTTP}SessionManager.{h,m}'
    ss.public_header_files = 'AFNetworking2/AF{URL,HTTP}SessionManager.h'
  end

  s.subspec 'UIKit' do |ss|
    ss.platform = :ios
    ss.dependency 'AFNetworking2/NSURLConnection'
    ss.dependency 'AFNetworking2/NSURLSession'

    ss.public_header_files = 'UIKit+AFNetworking2/*.h'
    ss.source_files = 'UIKit+AFNetworking2'
  end
end
